#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>

#define BAUD B38400

#include "../pins.h"

// {{{ MODELS

Model models[] = {
    { 
        .name = "28c256", 
        .pins = { A14, A12, A7, A6, A5, A4, A3, A2, A1, A0, D0, D1, D2, GND, NC,
                  NC, D3, D4, D5, D6, D7, CE, A10, RD, A11, A9, A8, A13, WR, VCC },
        .strategy = EEPROM,
        .size = 32 * 1024,
    },
    { 
        .name = "62256", 
        .pins = { A14, A12, A7, A6, A5, A4, A3, A2, A1, A0, D0, D1, D2, GND, NC,
                  NC, D3, D4, D5, D6, D7, CE, A10, RD, A11, A9, A8, A13, WR, VCC },
        .strategy = RAM,
        .size = 32 * 1024,
    },
};

// }}}

// {{{ OPTIONS

typedef struct {
    char*         port;
    char*         model;
    unsigned long input_bytes;
    bool          output;
    bool          test;
    char*         filename;
} Options;


static void show_help(char* cmd)
{
    printf("Usage: %s OPTIONS [FILENAME]\n", cmd);
    printf("Options:\n");
    printf("   -p PORT    USB port (ex. /dev/ttyUSB0)\n");
    printf("   -m MODEL   Memory model (ex. 62256)\n");
    printf("   -i SZ      Read SZ bytes from ROM\n");
    printf("   -o         Write file to ROM\n");
    printf("   -t         Test ROM (will destroy data)\n");
    printf("\n");
    printf("Supported models:\n");
    for (size_t i = 0; i < (sizeof models / sizeof(Model)); ++i) {
        printf("    %s\n", models[i].name);
    }
    exit(1);
}


static Options parse_options(int argc, char** argv)
{
    Options options = { NULL, NULL, 0, false, false, "" };

    int opt;
    while ((opt = getopt(argc, argv, "p:m:i:oth")) != -1) {
        switch (opt) {
            case 'p':
                options.port = optarg;
                break;
            case 'm':
                options.model = optarg;
                break;
            case 'i':
                options.input_bytes = atoi(optarg);
                break;
            case 'o':
                options.output = true;
                break;
            case 't':
                options.test = true;
                break;
            default:
                show_help(argv[0]);
                break;
        }
    }

    if (optind >= argc && !options.test) {
        fprintf(stderr, "Please choose a file.\n");
        show_help(argv[0]);
        exit(EXIT_FAILURE);
    } else if (!options.port) {
        fprintf(stderr, "Please select an USB port.\n");
        exit(EXIT_FAILURE);
    } else if (!options.model) {
        fprintf(stderr, "Please select a memory model.\n");
        exit(EXIT_FAILURE);
    } else if (options.input_bytes == 0 && !options.output && !options.test) {
        fprintf(stderr, "Please choose input, output or test.\n");
        exit(EXIT_FAILURE);
    }
    
    options.filename = argv[optind];

    return options;
}


// }}}

// {{{ SERIAL

static int serial_init(char* port)
{
    /* open port */
    int fd = open(port, O_RDWR  /* read/write */
            | O_NOCTTY          /* do not become the process controlling terminal */
            | O_SYNC);          /* synchronized I/O */
    if (fd < 0) {
        fprintf(stderr, "error opening port %s: %s\n", port, strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* get attributes */
    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0) {
        fprintf(stderr, "error getting attributes: %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
    }

    /* set attributes */
    cfsetospeed(&tty, BAUD);
    cfsetispeed(&tty, BAUD);

    tty.c_oflag &= ~(ONLCR | OCRNL);            /* don't change carriage return char */
    tty.c_iflag &= ~(INLCR | ICRNL);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; /* 8-bit */
    tty.c_iflag &= ~IGNBRK;                     /* disable break processing */
    tty.c_lflag = 0;                            /* no echo, no signaling chars, no canonical processing */
    tty.c_oflag = 0;                            /* no remapping, no delay */
    tty.c_cc[VMIN] = 1;                         /* read will block */
    tty.c_cc[VTIME] = 5;                        /* 0.5 seconds read timeout */
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);     /* shut off xon/xoff control */
    tty.c_cflag |= (CLOCAL | CREAD);            /* ignore modem controls, enable reading */
    tty.c_cflag &= ~(PARENB | PARODD);          /* disable parity */
    tty.c_cflag |= 0;                           /* no parity */
    tty.c_cflag &= ~CSTOPB;                     /* set stop bits */
    //tty.c_cflag &= ~CRTSCTS;                    /* Enable RTS/CTS (hardware) flow control */

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        fprintf(stderr, "error setting attributes: %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
    }

    return fd;
}


void send_byte(int fd, unsigned char byte)
{
    ssize_t r = write(fd, &byte, 1);
    if (r != 1) {
        fprintf(stderr, "Could not write to microcontroller: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}


unsigned char read_byte(int fd)
{
    unsigned char m;
    ssize_t r = read(fd, &m, 1);
    if (r != 1) {
        fprintf(stderr, "Could not read from microcontroller: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    return m;
}


static void send_model(int fd, Model const* model)
{
    for (int i = 0; i < BURNER_PINS; ++i) {
        send_byte(fd, model->pins[i]);
    }
    send_byte(fd, (unsigned char)model->strategy);
    send_byte(fd, (model->size >> 24) & 0xFF);
    send_byte(fd, (model->size >> 16) & 0xFF);
    send_byte(fd, (model->size >> 8) & 0xFF);
    send_byte(fd, model->size & 0xFF);

    unsigned char c;
    if ((c = read_byte(fd)) != (unsigned char)'x') {
        fprintf(stderr, "Unexpected response from uC: 0x%X\n", c);
        exit(EXIT_FAILURE);
    }
}


void send_buffer(int fd, unsigned char* buffer, size_t sz, size_t max_size)
{
    assert(sz <= max_size);
    printf("Writing 0x%zX bytes...", sz);

    send_byte(fd, 'w');
    send_byte(fd, (sz >> 24) & 0xFF);
    send_byte(fd, (sz >> 16) & 0xFF);
    send_byte(fd, (sz >> 8) & 0xFF);
    send_byte(fd, sz & 0xFF);

    for (size_t i=0; i < sz; ++i) {
        if ((i % 0x20) == 0) { printf("\n%08zX ", i); }
        if ((i % 0x8) == 0) { printf("  "); }
        printf("%02X ", buffer[i]);
        fflush(stdout);
        send_byte(fd, buffer[i]);

        // synchronization
        unsigned char c;
        if ((c = read_byte(fd)) != '$') {
            fprintf(stderr, "Error writing byte to ROM (got 0x%02X '%c').\n", c, (char)c);
            exit(EXIT_FAILURE);
        }
    }
    unsigned char c;
    if ((c = read_byte(fd)) != '!') {
        fprintf(stderr, "Error writing to ROM (got 0x%02X, '%c').\n", c, (char)c);
        exit(EXIT_FAILURE);
    }
    printf("\n");
}


void read_buffer(int fd, unsigned char* buffer, size_t sz)
{
    printf("Reading 0x%zX bytes...", sz);

    send_byte(fd, 'r');
    send_byte(fd, (sz >> 24) & 0xFF);
    send_byte(fd, (sz >> 16) & 0xFF);
    send_byte(fd, (sz >> 8) & 0xFF);
    send_byte(fd, sz & 0xFF);

    for (size_t i=0; i < sz; ++i) {
        buffer[i] = read_byte(fd);
        if ((i % 0x20) == 0) { printf("\n%08zX ", i); }
        if ((i % 0x8) == 0) { printf("  "); }
        printf("%02X ", buffer[i]);
        fflush(stdout);
    }
    printf("\n");
    if (read_byte(fd) != '!') {
        fprintf(stderr, "Error reading from ROM.\n");
        exit(EXIT_FAILURE);
    }
}


bool read_buffer_verify(int fd, unsigned char* verify, size_t sz)
{
    bool ok = true;

    send_byte(fd, 'r');
    send_byte(fd, (sz >> 24) & 0xFF);
    send_byte(fd, (sz >> 16) & 0xFF);
    send_byte(fd, (sz >> 8) & 0xFF);
    send_byte(fd, sz & 0xFF);

    printf("Verifying 0x%zX bytes...", sz);
    for (size_t i=0; i < sz; ++i) {
        char c = read_byte(fd);
        if ((i % 0x20) == 0) { printf("\n%08zX ", i); }
        if ((i % 0x8) == 0) { printf("  "); }
        if (c == verify[i]) { 
            //printf("%02X ", c);
            printf(".. ");
        } else {
            printf("%02X ", c);
            ok = false;
        }
        fflush(stdout);
    }
    printf("\n");

    if (read_byte(fd) != '!') {
        fprintf(stderr, "Error reading from ROM.\n");
        exit(EXIT_FAILURE);
    }

    return ok;
}

// }}}

// {{{ FILE MANAGEMENT
        
size_t read_whole_file(const char* filename, size_t max_sz, unsigned char** buffer)
{
    FILE* f = fopen(filename, "rb");
    if (!f) {
        fprintf(stderr, "Could not open file '%s'.\n", filename);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    long fsz = ftell(f);
    rewind(f);

    if ((unsigned long)fsz > max_sz) {
        fprintf(stderr, "File is larger (%ld kB) than IC memory space (%zu kB).\n", fsz/1024, max_sz/1024);
        exit(EXIT_FAILURE);
    }

    *buffer = malloc(fsz);
    fread(*buffer, fsz, 1, f);
    fclose(f);

    return fsz;
}


void write_whole_file(const char* filename, unsigned char* buffer, size_t sz)
{
    FILE *f = fopen(filename, "wb");
    if (!f) {
        fprintf(stderr, "Could not open file '%s'.\n", filename);
        exit(EXIT_FAILURE);
    }

    fwrite(buffer, sz, 1, f);
    fclose(f);
}


unsigned char* make_test_buffer(size_t sz)
{
    unsigned char* buffer = malloc(sz);
    for (size_t i=0; i<sz; ++i) {
        buffer[i] = (i & 0xFF);
    }
    return buffer;
}

// }}}

int main(int argc, char** argv)
{
    Options options = parse_options(argc, argv);

    // find model
    Model* model = NULL;
    for (size_t i = 0; i < (sizeof models / sizeof models[0]); ++i) {
        if (strcmp(options.model, models[i].name) == 0) {
            model = &models[i];
            break;
        }
    }
    if (!model) {
        fprintf(stderr, "Model %s not found. Call with '-h' to show all models.\n", options.model);
        exit(EXIT_FAILURE);
    }

    // show VCC/GND
    int vcc_pin, gnd_pin;
    for (int i=0; i < BURNER_PINS; ++i) {
        if (model->pins[i] == VCC) vcc_pin = i;
        if (model->pins[i] == GND) gnd_pin = i;
    }
    printf("Connect VCC cable (red) to pin %d and GND cable (black) to pin %d, and press ENTER.\n", vcc_pin, gnd_pin);
    getchar();

    // connect to uC and send/get data
    printf("Connecting to serial port %s...\n", options.port);
    int fd = serial_init(options.port);
    printf("Setting up model %s...\n", options.model);
    send_model(fd, model);

    if (options.output) {

        // write file
        unsigned char* buffer;
        size_t sz = read_whole_file(options.filename, model->size, &buffer);
        send_buffer(fd, buffer, sz, model->size);

        // verify
        if (read_buffer_verify(fd, buffer, sz)) {
            printf("Verification ok!\n");
        } else {
            printf("Verification failed :-(\n");
        }
        
        free(buffer);

    } else if (options.input_bytes) {

        if (options.input_bytes > model->size) {
            fprintf(stderr, "Can't read more than %lld kB for this memory.\n", model->size);
            exit(EXIT_FAILURE);
        }

        // read file
        unsigned char* buffer = malloc(options.input_bytes);
        read_buffer(fd, buffer, options.input_bytes);
        write_whole_file(options.filename, buffer, options.input_bytes);
        free(buffer);

    } else if (options.test) {

        unsigned char* buffer = make_test_buffer(model->size);

        // write buffer
        send_buffer(fd, buffer, model->size, model->size);

        // read buffer
        if (read_buffer_verify(fd, buffer, model->size)) {
            printf("Verification ok!\n");
        } else {
            printf("Verification failed :-(\n");
        }
        
        free(buffer);

    } else {
        abort();

    }

    send_byte(fd, 'x');  // finalize execution
}

/* vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
 */
