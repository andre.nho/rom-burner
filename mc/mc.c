#define F_CPU 16000000
#define BAUD 38400

#include <stdbool.h>

#include <avr/io.h>
#include <avr/cpufunc.h>
#include <util/delay.h>
#include <util/setbaud.h>

#include "../pins.h"

#define WAIT { \
    _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); \
    _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); \
    _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); \
    _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); _NOP(); \
}

// {{{ UART

static void 
uart_init()
{
    // set speed
    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA &= ~(_BV(U2X));
#else
    UCSRA &= ~(_BV(U2X));
#endif

    UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0); // 8-bit data
    UCSRB = _BV(RXEN) | _BV(TXEN);  // enable RX and TX
}

void tx(uint8_t c)
{
    while ( !(UCSRA & _BV(UDRE)) );  // wait until last byte is written
    UDR = c;
}


uint8_t rx()  // blocking
{
    while (!(UCSRA & _BV(RXC)));
    return UDR;
}

// }}}

// {{{ PINS

/*
DDR PORT
 0    0  - input, high impedance
 0    1  - input, pulled up
 1    0  - output, driven low
 1    1  - output, driven high
*/

static void 
setup_pinage(Model* model, int8_t pinage[N_PINS])
{
    for (unsigned int i=0; i < N_PINS; ++i) {
        pinage[i] = -1;
        for (int j=0; j < BURNER_PINS; ++j) {
            if (model->pins[j] == i) {
                pinage[i] = j;
            }
        }
    }
}


static void 
set_pin_mode(int8_t pinage[N_PINS], Pin tpin, char mode)
{
    int8_t pin = pinage[tpin];

    if (pin < 0) {
        return;
    } else if (pin <= 7) {
        if (mode == 'w') {
            DDRA |= (1 << pin);
        } else {
            DDRA &= ~(1 << pin);
        }
        _NOP();
        PORTA &= ~(1 << pin);
    } else if (pin <= 15) {
        int p = 15 - pin;
        if (mode == 'w') {
            DDRC |= (1 << p);
        } else {
            DDRC &= ~(1 << p);
        }
        _NOP();
        PORTC &= ~(1 << p);
    } else if (pin <= 21) {
        int p = 23 - pin;
        if (mode == 'w') {
            DDRD |= (1 << p);
        } else {
            DDRD &= ~(1 << p);
        }
        _NOP();
        PORTD &= ~(1 << p);
    } else {
        int p = 29 - pin;
        if (mode == 'w') {
            DDRB |= (1 << p);
        } else {
            DDRB &= ~(1 << p);
        }
        _NOP();
        PORTB &= ~(1 << p);
    }
}


static bool
get_pin(int8_t pinage[N_PINS], Pin tpin)
{
    int8_t pin = pinage[tpin];

    if (pin < 0) {
        return false;
    } else if (pin <= 7) {
        return (PINA >> pin) & 1;
    } else if (pin <= 15) {
        int p = 15 - pin;
        return (PINC >> p) & 1;
    } else if (pin <= 21) {
        int p = 23 - pin;
        return (PIND >> p) & 1;
    } else {
        int p = 29 - pin;
        return (PINB >> p) & 1;
    }
}


static void
set_pin(int8_t pinage[N_PINS], Pin tpin, bool value)
{
    int8_t pin = pinage[tpin];

    if (pin < 0) {
    } else if (pin <= 7) {
        if (value) {
            PORTA |= (1 << pin);
        } else {
            PORTA &= ~(1 << pin);
        } 
    } else if (pin <= 15) {
        int p = 15 - pin;
        if (value) {
            PORTC |= (1 << p);
        } else {
            PORTC &= ~(1 << p);
        }
    } else if (pin <= 21) {
        int p = 23 - pin;
        if (value) {
            PORTD |= (1 << p);
        } else {
            PORTD &= ~(1 << p);
        }
    } else {
        int p = 29 - pin;
        if (value) {
            PORTB |= (1 << p);
        } else {
            PORTB &= ~(1 << p);
        }
    }
}


static void
prepare_pins(int8_t pinage[N_PINS], char action)
{
    if (action == 'h') {
        DDRA = DDRB = DDRC = DDRD = 0;
        _NOP();
        PORTA = PORTB = PORTC = PORTD = 0;
    } else {
        set_pin_mode(pinage, VCC, 'h');
        set_pin_mode(pinage, GND, 'h');
        set_pin_mode(pinage, CE, 'w'); set_pin(pinage, CE, true);
        set_pin_mode(pinage, RD, 'w'); set_pin(pinage, RD, true);
        set_pin_mode(pinage, WR, 'w'); set_pin(pinage, WR, true);
        for (int i=0; i<16; ++i) {
            set_pin_mode(pinage, A0+i, 'w');
        }
        for (int i=0; i<8; ++i) {
            set_pin_mode(pinage, D0+i, action);
        }
    }
}


// }}}

// {{{ STRATEGY: PARALLEL

static void
parallel_set_address(int8_t pinage[N_PINS], uint64_t addr)
{
    for (int i=0; i<16; ++i) {
        set_pin(pinage, A0 + i, (addr >> i) & 1);
    }
}

static void
parallel_set_data(int8_t pinage[N_PINS], uint8_t value)
{
    for (int i=0; i<8; ++i) {
        set_pin_mode(pinage, D0 + i, 'w');
    }

    for (int i=0; i<8; ++i) {
        set_pin(pinage, D0 + i, (value >> i) & 1);
    }
}

static uint8_t
parallel_get_data(int8_t pinage[N_PINS])
{
    for (int i=0; i<8; ++i) {
        set_pin_mode(pinage, D0 + i, 'r');
    }

    uint8_t data = 0;
    for (int i=0; i<8; ++i) {
        data |= (uint8_t)get_pin(pinage, D0 + i) << i;
    }
    return data;
}

// }}}

// {{{ STRATEGY: RAM (PARALLEL)

static uint8_t 
ram_read(int8_t pinage[N_PINS], uint64_t addr)
{
    set_pin(pinage, WR, true);

    // setup address
    parallel_set_address(pinage, addr);
    set_pin(pinage, CE, false);
    set_pin(pinage, RD, false);

    // read data
    WAIT;
    uint8_t data = parallel_get_data(pinage);

    // finalize
    set_pin(pinage, RD, true);
    set_pin(pinage, CE, true);
    WAIT;

    return data;
}

static void 
ram_burn(int8_t pinage[N_PINS], uint64_t addr, uint8_t value)
{
    set_pin(pinage, RD, true);

    // write data
    parallel_set_address(pinage, addr);
    set_pin(pinage, CE, false);
    set_pin(pinage, WR, false);
    parallel_set_data(pinage, value);
    WAIT;

    // finalize
    set_pin(pinage, WR, true);
    set_pin(pinage, CE, true);
    WAIT;
}

// }}}

// {{{ STRATEGY: EEPROM (PARALLEL)

static void 
eeprom_burn(int8_t pinage[N_PINS], uint64_t addr, uint8_t value)
{
    // write data
    set_pin(pinage, RD, true);
    set_pin(pinage, WR, true);
    set_pin(pinage, CE, false);
    parallel_set_address(pinage, addr);
    WAIT;
    set_pin(pinage, WR, false);
    WAIT;
    parallel_set_data(pinage, value);
    WAIT;
    set_pin(pinage, WR, true);
    WAIT;
    set_pin(pinage, CE, true);
    WAIT;

    // finalize
    set_pin(pinage, CE, true);
    set_pin(pinage, RD, true);
    WAIT;

    // keeps polling until the correct byte is returned
    while (ram_read(pinage, addr) != value) {}
}

// }}}

// {{{ COMMUNICATION

static Model
get_model()
{
    Model model;

    for (int i=0; i<BURNER_PINS; ++i) {
        model.pins[i] = rx();
    }
    model.strategy = rx();

    model.size = 0;
    model.size |= ((uint64_t)rx() << 24);
    model.size |= ((uint64_t)rx() << 16);
    model.size |= ((uint64_t)rx() << 8);
    model.size |= (uint64_t)rx();

    tx('x');

    return model;
}


static void 
read_uart_and_burn(Model const* model, int8_t pinage[N_PINS])
{
    uint64_t sz = ((uint64_t)rx() << 24);
    sz |= ((uint64_t)rx() << 16);
    sz |= ((uint64_t)rx() << 8);
    sz |= rx();

    for (uint64_t i=0; i < sz; ++i) {
        switch (model->strategy) {
            case EEPROM:
                eeprom_burn(pinage, i, rx());
                break;
            case RAM:
                ram_burn(pinage, i, rx());
                break;
        }
        // synchronization
        tx('$');
    }
    tx('!');
}


static void 
read_mem_and_send(Model const* model, int8_t pinage[N_PINS])
{
    uint64_t sz = ((uint64_t)rx() << 24);
    sz |= ((uint64_t)rx() << 16);
    sz |= ((uint64_t)rx() << 8);
    sz |= rx();

    for (uint64_t i=0; i < sz; ++i) {
        switch (model->strategy) {
            case EEPROM:
            case RAM:
                tx(ram_read(pinage, i));
                break;
        }
    }
    tx('!');
}

// }}}

int main()
{
    int8_t pinage[N_PINS];

    uart_init();
    prepare_pins((void*)0, 'h');
    
next:;
    Model model = get_model();
    setup_pinage(&model, pinage);
    while(1) {
        switch (rx()) {
            case 'w':
                prepare_pins(pinage, 'w');
                read_uart_and_burn(&model, pinage);
                break;

            case 'r':
                prepare_pins(pinage, 'r');
                read_mem_and_send(&model, pinage);
                break;

            case 'x':
                prepare_pins(pinage, 'h');
                goto next;
        }
    }
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
