#define BURNER_PINS 30

typedef enum {
    NC=0,
    D0, D1, D2, D3, D4, D5, D6, D7,
    A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15, A16, A17, A18,
    RD, WR, CE, VCC, GND,
    N_PINS,
} Pin;

typedef enum {
    EEPROM, RAM, /* SERIAL_EEPROM, */
} Strategy;

typedef struct {
    char*    name;
    Pin      pins[BURNER_PINS];
    Strategy strategy;
    unsigned long long size;
} Model;
